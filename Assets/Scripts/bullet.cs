﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet: MonoBehaviour {


    float speedBullet =1;
    public Rigidbody2D rb;
    // Use this for initialization
    void Start () {
        //speedBullet = 8;
        rb = GetComponent<Rigidbody2D>();

    }
	
	// Update is called once per frame
	void Update () {
        //Vector2 position = transform.position;
        //position = new Vector2(position.x, position.y + speedBullet * Time.deltaTime);
        //transform.position = position;
        rb.AddForce(transform.up * speedBullet);
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        if(transform.position.y > max.y)
        {
            Destroy(gameObject);
        }
		
	}
}
