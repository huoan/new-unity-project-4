﻿/*/
* Script by Devin Curry
* http://Devination.com
* https://youtube.com/user/curryboy001
* Please like and subscribe if you found my tutorials helpful :D
* Twitter: https://twitter.com/Devination3D
/*/
using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class FloatingPlayer2DController : MonoBehaviour
{
	public float moveForce = 5, boostMultiplier = 2;
	Rigidbody2D myBody;

    float count = 5;
    int limit = 0;
    bool contador = false;


    void Start ()
	{
		myBody = this.GetComponent<Rigidbody2D>();
	}
	


	void FixedUpdate ()
	{
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (touch.position.x < Screen.width / 2)
                {
                    Debug.Log("Left click" + moveForce);
                    moveForce = 5;
                    contador = true;

                }
                else if (touch.position.x > Screen.width / 2)
                {
                    Debug.Log("Right click");
                }
            }
        }
        if (contador)
        {
            moveForce -= Time.deltaTime;
            if (count >= 0)
            {
                count = 0;
                contador = false;
                Debug.LogError(moveForce);
            }
        }



        Vector2 moveVec = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"),
			CrossPlatformInputManager.GetAxis("Vertical"))
			* moveForce;
		Vector3 lookVec = new Vector3(CrossPlatformInputManager.GetAxis("Horizontal_2"),
			CrossPlatformInputManager.GetAxis("Vertical_2"), 4096);

		if (lookVec.x != 0 && lookVec.y != 0)
			transform.rotation = Quaternion.LookRotation(lookVec, Vector3.back);
		
		bool isBoosting = CrossPlatformInputManager.GetButton("Boost");
		myBody.AddForce(moveVec * (isBoosting ? boostMultiplier : 1));
	}
}